package StepDefinations;

import org.junit.runner.RunWith;

import io.cucumber.junit.Cucumber;
import io.cucumber.testng.AbstractTestNGCucumberTests;
import io.cucumber.testng.CucumberOptions;
import io.cucumber.testng.CucumberOptions.SnippetType;
@RunWith(Cucumber.class)
@CucumberOptions(
		features = {"/home/ranjith/eclipse-workspace/DemoWebShop_Assiginment/src/test/resources/DeoWebshops"},
		glue = "StepDefinations",
		snippets = SnippetType.CAMELCASE,
		monochrome = true,
		plugin = {"pretty",
				"html:target/cucumber-reports"}
		
				
	
	   )
public class RunnerClasss extends AbstractTestNGCucumberTests {
}
